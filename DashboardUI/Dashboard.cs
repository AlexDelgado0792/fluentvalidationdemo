﻿
using ModelLibrary;
using System;
using System.ComponentModel;
using System.Windows.Forms;
using ModelLibrary.Validations;

namespace DashboardUI
{
  public partial class Dashboard : Form
  {
    BindingList<string> errors = new BindingList<string>();

    public Dashboard()
    {
      InitializeComponent();

      errorListBox.DataSource = errors;
    }

    private void createButton_Click(object sender, EventArgs e)
    {
      errors.Clear();

      if (!decimal.TryParse(accountBalanceText.Text, out decimal accountBalance))
      {
        errors.Add("Account Balance: Invalid Amount");
        return;
      }

      PersonModel person = new PersonModel();
      person.FirstName = firstNameText.Text;
      person.LastName = lastNameText.Text;
      person.AccountBalance = accountBalance;
      person.DateOfBirth = dateOfBirthPicker.Value;

      //Validate Data
      var validator = new PersonValidator();

      var result = validator.Validate(person);

      if (result.IsValid == false)
      {
          foreach (var failure in result.Errors)
          {
              errors.Add($"{failure.ErrorMessage}");
          }
      }

      // Insert into the database

      MessageBox.Show("Operation Complete");
    }
  }
}
