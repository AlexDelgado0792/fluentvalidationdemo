﻿
using System;
using System.Linq;
using FluentValidation;

namespace ModelLibrary.Validations
{
    public class PersonValidator: AbstractValidator<PersonModel>
    {
        public PersonValidator()
        {
            RuleFor(x => x.FirstName)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotEmpty().WithMessage("{PropertyName} is Empty.")
                .Length(2, 50).WithMessage("Lenght ({TotalLength}) of {PropertyName} invalid.")
                .Must(BeValidName).WithMessage("{PropertyName} contains Invalid Characters.");

            RuleFor(x => x.LastName)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotEmpty().WithMessage("{PropertyName} is Empty.")
                .Length(2, 50).WithMessage("Lenght ({TotalLength}) of {PropertyName} invalid.")
                .Must(BeValidName).WithMessage("{PropertyName} contains Invalid Characters.");
            
            RuleFor(p => p.DateOfBirth)
                .Must(BeValidAge).WithMessage("Invalid {PropertyName}.");
        }

        protected bool BeValidAge(DateTime date)
        {
            int currentYear = DateTime.UtcNow.Year;
            int dobYear = date.Year;


            if (dobYear <= currentYear && dobYear > (currentYear - 120))
            {
                return true;
            }

            return false;
        }

        protected bool BeValidName(string name)
        {
            name = name.Replace(" ", string.Empty);
            name = name.Replace("-", string.Empty);

            return name.All(char.IsLetter);
        }
    }
}
